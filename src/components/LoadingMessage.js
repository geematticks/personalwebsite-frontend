import React, {Component} from 'react';
import {Glyphicon} from 'react-bootstrap'

class LoadingMessage extends Component {

    render() {
        return (
            <div style={{padding: '10px'}} className={this.props.isLoading ? null : 'hidden' }>
                <p className="center-white-text"><Glyphicon glyph="refresh" className="rotation"/> {this.props.message}</p>
            </div>
        );
    }
}

export default LoadingMessage;