import React, {Component} from 'react';
import {Alert} from 'react-bootstrap'

class DisplayMessage extends Component {

    render() {
        return (
            <div style={{paddingTop: '10px', paddingBottom: '10px'}} className={this.props.messageType && this.props.message ? null : 'hidden' }>
                <Alert bsStyle={this.props.messageType}>
                    <div>
                        <p style={{display: 'inline-block', marginRight: '10px'}}>{this.props.message}</p>
                        <button className="btn btn-default" onClick={this.props.onDismiss}>X</button>
                    </div>
                </Alert>
            </div>
        );
    }

}

export default DisplayMessage;