import React, {Component} from 'react';
import $ from 'jquery'
import 'jquery.appear';
import FontAwesome from 'react-fontawesome'

import HomeScreenCircle from './HomeScreenCircle';

import circleImg1 from '../../assets/img/circle.png';
import circleFillingImg from '../../assets/img/circle_filling.png';
import circleFilling2Img from '../../assets/img/circle_filling_2.png';
import line1Img from '../../assets/img/line1.png';
import line2Img from '../../assets/img/line2.png';
import line3Img from '../../assets/img/line3.png';
import line4Img from '../../assets/img/line4.png';
import websiteTitleImg from '../../assets/img/website_title.png';

class HomeScreen extends Component {

    render() {
        return (
            <div style={{height: '100vh'}}>
                <div id="logo-container">
                    <div className="logo">
                        <HomeScreenCircle img={circleFilling2Img} id="maincircle" width="420px" top="-210px"
                                          left="-210px"
                                          opacity="1.0"/>
                        <HomeScreenCircle img={circleFillingImg} id="circle1" width="200px" top="-350px" left="-350px"
                                          opacity="1.0"/>
                        <HomeScreenCircle img={circleFillingImg} id="circle2" width="130px" top="-250px" left="200px"
                                          opacity="1.0"/>
                        <HomeScreenCircle img={circleImg1} id="circle4" width="100px" top="-60px" left="350px"
                                          opacity="1.0"/>
                        <HomeScreenCircle img={circleFillingImg} id="circle5" width="60px" top="-150px" left="520px"
                                          opacity="1.0"/>

                        <img src={line1Img} id="line1"
                             style={{marginLeft: '-212px', marginTop: '-192px', opacity: '1.0'}}
                             className="center"/>
                        <img src={line2Img} id="line2"
                             style={{marginLeft: '139px', marginTop: '-167px', opacity: '1.0'}}
                             className="center"/>
                        <img src={line3Img} id="line3"
                             style={{marginLeft: '280px', marginTop: '-154px', opacity: '1.0'}}
                             className="center"/>
                        <img src={line4Img} id="line4"
                             style={{marginLeft: '424px', marginTop: '-113px', opacity: '1.0'}}
                             className="center"/>
                    </div>

                    <div id="logo-title" className="center main-title">
                        <div className="website-title-holder">
                            <img src={websiteTitleImg}/>
                        </div>

                        <div className="social-network-links">
                            <a href="https://www.linkedin.com/in/gmatticks/" target="_blank">
                                <FontAwesome name='linkedin-square' size='4x'/>
                            </a>
                            <a href="https://www.instagram.com/gmatticks/?hl=en" target="_blank">
                                <FontAwesome name='instagram' size='4x'/>
                            </a>
                            <a href="https://gitlab.com/geematticks" target="_blank">
                                <FontAwesome name='gitlab' size='4x'/>
                            </a>
                            <a href="https://www.youtube.com/channel/UCDGQJCQ-hNuHXRUsW4UXFGg" target="_blank">
                                <FontAwesome name='youtube' size='4x'/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        let that = this;

        $('.logo > img, #logo-title').css('visibility', 'hidden');
        $('#maincircle, #circle1, #circle2, #circle4, #circle5').css({
            '-moz-transform': 'scale(0)',
            '-webkit-transform': 'scale(0)'
        });

        $(window).on("load", function () {
            that.zoomAnimation('#circle1', 300);
            that.fadeInAnimation('#circle1', 300);

            that.zoomAnimation('#maincircle', 550);
            that.fadeInAnimation('#maincircle', 550);

            that.zoomAnimation('#circle2', 750);
            that.fadeInAnimation('#circle2', 750);

            that.zoomAnimation('#circle4', 900);
            that.fadeInAnimation('#circle4', 900);

            that.zoomAnimation('#circle5', 1100);
            that.fadeInAnimation('#circle5', 1100);

            that.fadeInAnimation('#line1', 1000);
            that.fadeInAnimation('#line2', 1100);
            that.fadeInAnimation('#line3', 1200);
            that.fadeInAnimation('#line4', 1350);
            that.fadeInAnimation('#logo-title', 800);
        });
    }

    /* Animation methods */
    zoomAnimation(elelementSelector, delay) {
        setTimeout(function () {
            $(elelementSelector).css({
                '-moz-transform': 'scale(1)',
                '-webkit-transform': 'scale(1)',
                '-webkit-transition': 'all .9s ease-out',
                '-moz-transition': 'all .9s ease-out',
                '-o-transition': 'all .9s ease-out',
                'transition': 'all .9s ease-out'
            });
        }, delay);
    }

    fadeInAnimation(elementSelector, delay) {
        setTimeout(function () {
            $(elementSelector).css('visibility', 'visible').hide().fadeIn(900);
        }, delay);
    }
}

export default HomeScreen;