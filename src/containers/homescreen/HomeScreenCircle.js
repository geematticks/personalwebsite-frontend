import React, {Component} from 'react';

class HomeScreenCircle extends Component {

    render() {
        return (
            <img src={this.props.img} id={this.props.id} style={{width: this.props.width, marginTop: this.props.top, marginLeft: this.props.left, opacity: this.props.opacity}} className="center"/>
        );
    }
}

export default HomeScreenCircle;