import axios from 'axios'

class HttpUtilService {
    constructor() {
        this.axiosClient = axios.create({
            baseURL: process.env.REACT_APP_BACK_END_API,
            headers: {'Content-Type':'application/json'}
        });
    }

    post(url, body) {
        return this.axiosClient.post(url, body);
    }
}

let service = new HttpUtilService();
export default service;