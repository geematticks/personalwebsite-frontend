import React, {Component} from 'react';
import {Col, Row} from 'react-bootstrap'
import FontAwesome from 'react-fontawesome'
import $ from 'jquery';
import 'jquery.appear';

import ExperienceComponent from './ExperienceComponent';
import aboutMeTitle from '../../assets/img/about_me_title.png';

class AboutMe extends Component {
    render() {
        return (
            <div>
                <Row style={{width: '100%', paddingBottom: '10px'}}>
                    <Col lg={3} md={1} sm={0}/>
                    <Col lg={6} md={10} sm={12}>
                        <div className="title-holder">
                            <img src={aboutMeTitle} id="title" className="title" alt="experience"/>
                        </div>

                        <p className="center-black-text">
                            London living, technology enthusiast.
                            Front to Back-end developer and beyond.<br/>
                            Strong background working in the finance industry over areas such as risk & pricing, trade
                            processing and reference data.<br/>
                            Proponent of the Agile methodology and certified Scrum practitioner.
                        </p>

                        <div className="timeline-holder">
                            <div id="experience-hsbc-2-icon" className="timeline">
                                <FontAwesome name='building' size='2x'/>
                                <p>HSBC (Present)</p>
                            </div>

                            <div id="experience-morgan-icon" className="timeline">
                                <FontAwesome name='building' size='2x'/>
                                <p>Morgan Stanley (2021)</p>
                            </div>

                            <div id="experience-digiterre-icon" className="timeline">
                                <FontAwesome name='building' size='2x'/>
                                <p>Digiterre (2021)</p>
                            </div>

                            <div id="experience-ebay-icon" className="timeline">
                                <FontAwesome name='building' size='2x'/>
                                <p>eBay (2019)</p>
                            </div>

                            <div id="experience-hsbc-icon" className="timeline">
                                <FontAwesome name='building' size='2x'/>
                                <p>HSBC (2019)</p>
                            </div>

                            <div id="experience-cme-icon" className="timeline">
                                <FontAwesome name='building-o' size='2x'/>
                                <p>CME (2017)</p>
                            </div>
                        </div>

                        <div className="experience-holder-container">
                            <div className="experience-holder-div">
                                <div id="no-click" className="experience-holder">
                                    <div className="experience-component-title-holder">
                                        <h3 style={{textAlign: 'center', fontWeight: 'bold'}}>Click on each item above
                                            for
                                            more information.</h3>
                                    </div>
                                </div>
                            </div>

                            <ExperienceComponent id="experience-hsbc-2" title="Derivatives Technical Senior Developer"
                                                 company="HSBC"
                                                 points={[
                                                     'Development duties for Equities Derivatives risk calculation distributed system (VaR, HVaR, Stress VaR)',
                                                     'Implemented conversion from legacy Solace JMS to Google cloud PubSub message publishing for critical area of application (resulting in improved message throughput, less production incidents and less infrastructure overhead)',
                                                     'Successfully rewritten and migrated Equities risk scenario VAR collection, calculation and publication to Google Cloud Dataflow (serverless) pipeline'
                                                 ]}
                                                 stack="Java 17, Spring Boot, Apache Beam, Google Cloud Platform (Compute Engine, Dataflow, BigQuery, PubSub), Terraform"/>

                            <ExperienceComponent id="experience-morgan" title="Java Developer"
                                                 company="Morgan Stanley"
                                                 points={[
                                                     'Development duties for client facing applications to support the Equity structured product business, allowing price quoting, trade execution & document generation for clients of Morgan Stanley via Email, REST & FIX connectivity (Java 8, Spring Boot, Jenkins, Microservices, REST, FIX, SOAP)',
                                                     'Responsibilities supporting new developers during on-boarding & responsibility conducting interviews for technical hires.',
                                                     'Supported go-live and the resolution of production issues. This allowed the execution of the first sets of trades with a Swiss client via new Structured products quoting and execution system.',
                                                 ]}
                                                 stack="Java, Spring Boot, FIX, SOAP, Kafka"/>

                            <ExperienceComponent id="experience-morgan" title="Java Developer"
                                                 company="Morgan Stanley"
                                                 points={[
                                                     'Development duties for client facing applications to support the Equity structured product business, allowing price quoting, trade execution & document generation for clients of Morgan Stanley via Email, REST & FIX connectivity (Java 8, Spring Boot, Jenkins, Microservices, REST, FIX, SOAP)',
                                                     'Responsibilities supporting new developers during on-boarding & responsibility conducting interviews for technical hires.',
                                                     'Supported go-live and the resolution of production issues. This allowed the execution of the first sets of trades with a Swiss client via new Structured products quoting and execution system.',
                                                 ]}
                                                 stack="Java, Spring Boot, FIX, SOAP, Kafka"/>

                            <ExperienceComponent id="experience-digiterre" title="Senior Consultant Software Developer"
                                                 company="Digiterre"
                                                 points={[
                                                     'Full stack development duties for settlement system within leading energy trading client',
                                                     '1st line support for middle office users relating to production issues, conducting demos, requirement gathering and UAT support',
                                                     'Improved release process by creating script to automatically tag fix versions for Jira user stories based on Git commit history',
                                                     'Introduced Cucumber BDD tests to capture supported settlement scenarios'
                                                 ]}
                                                 stack="Java, DropWizard, TypeScript, AngularJS, Solace JMS, SQL Server"/>

                            <ExperienceComponent id="experience-ebay" title="Full Stack Software Engineer"
                                                 company="eBay"
                                                 points={[
                                                     'Front/Back-end software development duties for eBay\'s Bulk seller portal (Java 8, Spring Boot, ReactJS, Typescript, Redux, Sass)',
                                                     'Handled the migration of international shipping for label printing portal, including handing the of production roll-out.',
                                                     'Responsible for the end to end implementation of the Australia Bulk label printing portal.',
                                                     'Initialized new single label printing migration project incorporating Typescript.']}
                                                 stack="Java, Kotlin, TypeScript, Spring Boot, Redux, ReactJS"/>

                            <ExperienceComponent id="experience-hsbc" title="Back End Developer"
                                                 company="HSBC Retail Banking and Wealth Management"
                                                 points={[
                                                     'Server side developer duties for HSBC in-branch tablet application (Java 8, Kotlin, Spring Boot, Reactive Framework, Micro Service Architecture, MongoDB, Jenkins, Pivotal Cloud Foundry deployments)',
                                                     'Led implementation for Metric Dashboard for HSBC Branch staff user and Walk Ins/Pre-Booked Appointment tracking',
                                                     'Created B/E Jenkins pipeline plans for CI Testing & Automated deployment.',
                                                     'Converted Cloud Foundry deployments scripts to blue green deployments, enabling releases with no downtime, to minimize impact to application users, other dependent developers and testers.',]}
                                                 stack="Java, Kotlin, GraphQL, Spring Web Flux, Project Reactor, MongoDB, Cucumber, JMeter, Pivotal Cloud Foundry"/>

                            <ExperienceComponent id="experience-cme" title="Senior Software Engineer"
                                                 company="CME Group"
                                                 points={[
                                                     'Full stack development duties for Risk and Pricing applications.',
                                                     'Made improvements to trade adapter application by suggesting and implementing \'Protobuf serialization\' ' +
                                                     'to reduce memory footprint by 33 times solving a serious production issue and halving serialization times.',
                                                     'Created File Generator module responsible for building reference data files for contract pricer component' +
                                                     ' using file streaming to keep memory footprint low for very large files.',
                                                     'Implemented conversion to Kafka messaging component for trade adapter application/CME trading venue codebase.',
                                                     'Fully developed historical scenarios reference data module working with Clearing Risk Analysts for Futures/Options/OTC products P&L and Risk pricing.']}
                                                 stack="Java, JavaScript, AngularJS, Oracle SQL, MongoDB, Maven, Git, Stash, Bamboo"/>
                        </div>
                    </Col>
                    <Col lg={3} md={1} sm={0}/>
                </Row>
            </div>
        );
    }

    componentDidMount() {
        //hide all
        $('.experience-holder').hide();
        $('#no-click').show();

        this.closePreviousDisplay();

        this.setExperienceClickHandlers();

        this.animateTimelineButtonsWhenInView();

        this.animateExperienceContainerInView();
    }

    setExperienceClickHandlers() {
        const transitionSpeed = 200;

        ['#experience-hsbc-2', '#experience-morgan', '#experience-cme', '#experience-hsbc', '#experience-ebay', '#experience-digiterre']
            .forEach(id => {
                let iconId = id + '-icon';
                $(iconId).click(function () {
                    $(iconId).addClass('item-selected');
                    setTimeout(function () {
                        $(id).fadeIn(transitionSpeed)
                    }, transitionSpeed);
                });

            });
    }

    /* Animation methods */
    closePreviousDisplay() {
        let switchTime = 200;

        $('.timeline').click(function () {
            $('.experience-holder').fadeOut(switchTime);
            $('#no-click').fadeOut(switchTime);
            $('.timeline').removeClass('item-selected');
        });
    }

    animateTimelineButtonsWhenInView() {
        let timelineSelector = $('.timeline');
        timelineSelector.css({
            '-moz-transform': 'scale(0)',
            '-webkit-transform': 'scale(0)'
        });
        timelineSelector.appear(function () {
            let transition = (Math.random() * (0.6 - 1.4) + 1.4).toFixed(4);
            $(this).css({
                '-moz-transform': 'scale(1)',
                '-webkit-transform': 'scale(1)',
                '-webkit-transition': 'all ' + transition + 's ease-out',
                '-moz-transition': 'all ' + transition + 's ease-out',
                '-o-transition': 'all ' + transition + 's ease-out',
                'transition': 'all ' + transition + 's ease-out',
            });
        });
    }

    animateExperienceContainerInView() {
        let selector = $('.experience-holder-container');
        selector.css('visibility', 'hidden');
        selector.appear(function () {
            $(this).css('visibility', 'visible').hide().fadeIn(1000);
        });
    }

}

export default AboutMe;