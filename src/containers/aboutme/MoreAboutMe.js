import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap'

import moreAboutMeTitle from '../../assets/img/moreaboutme_title.png';

class MoreAboutMe extends Component {
    render() {
        return (
            <div className="dark-page">
                <Row style={{width: '100%'}}>
                    <Col lg={3} md={1} sm={0}/>
                    <Col lg={6} md={10} sm={12}>
                        <div className="title-holder">
                            <img src={moreAboutMeTitle} id="title" className="title"/>
                        </div>

                        <p style={{color: 'white', textAlign: 'center'}}>
                            When I'm not programming. I'm (very infrequently) producing instrumental music.<br/>
                            Take a look at some of my stuff here (If you're into that sort of thing of course!)
                        </p>

                        <div>
                            <iframe width="100%" height="130" scrolling="no" frameborder="no"
                                    src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/148005870&amp;color=%230066cc&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                        </div>

                        <div>
                            <iframe width="100%" height="130" scrolling="no" frameborder="no"
                                    src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/132552701&amp;color=%230066cc&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                        </div>

                        <div>
                            <iframe width="100%" height="130" scrolling="no" frameborder="no"
                                    src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/95906539&amp;color=%230066cc&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                        </div>
                    </Col>
                    <Col lg={3} md={1} sm={0}/>
                </Row>
            </div>
        );
    }
}

export default MoreAboutMe;