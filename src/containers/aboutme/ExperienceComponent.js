import React, {Component} from 'react';

class ExperienceComponent extends Component {
    render() {
        const listItems = this.props.points.map((point, index) =>
            <li key={index}>{point}</li>
        );
        return (

            <div className="experience-holder-div">
                <div id={this.props.id} className="experience-holder">
                    <div className="experience-component-title-holder">
                        <h3 style={{textAlign: 'center', fontWeight: 'bold'}}>{this.props.title}</h3>
                        <h4 style={{textAlign: 'center', color: 'rgba(220, 220, 220)'}}>{this.props.company}</h4>
                    </div>

                    <ul>
                        {listItems}
                    </ul>

                    {this.props.stack && <div style={{paddingLeft: 20, marginTop: 10}}>
                        <p style={{color: 'yellow'}}>Technical Stack: {this.props.stack}</p>
                    </div>}
                </div>
            </div>
        );
    }
}

export default ExperienceComponent;