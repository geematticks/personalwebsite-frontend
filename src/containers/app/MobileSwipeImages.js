import React from "react";
import mobileSwipeImageTitle from "../../assets/img/shopify-mobile-swipe-images.png";
import {Col, Row} from "react-bootstrap";
import privacyPolicy from "../../assets/img/privacy_policy.png";

export const MobileSwipeImages = () => {
    return (
        <div>
            <div>
                <div className="title-holder">
                    <img src={mobileSwipeImageTitle} id="title" className="title" alt="experience"/>
                </div>

                <Row style={{width: '100%', paddingTop: '10px', paddingBottom: '10px'}}>
                    <Col lg={3} md={1} sm={0}/>
                    <Col lg={6} md={10} sm={12}>
                        <div style={{minHeight: '100px'}}>
                            <div className="iframe-container">
                                <iframe id="ytplayer"
                                        src="https://www.youtube.com/embed/3T6g95X4X44?autoplay=0&rel=0&origin=http://https://www.gerronematticks.co.uk"
                                        frameBorder="0"/>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={1} sm={0}/>
                </Row>
            </div>

            <div id="privacy-policy" className='dark-page' style={{minHeight: 'auto'}}>
                <Row style={{width: '100%', paddingTop: '10px', paddingBottom: '10px'}}>
                    <Col lg={3} md={1} sm={0}/>
                    <Col lg={6} md={10} sm={12}>
                        <div className="project-description">
                            <div>
                                <h2>SMART PRODUCT IMAGE SWIPER PRIVACY POLICY</h2>
                            </div>
                            <div>
                                SMART PRODUCT IMAGE SWIPER "the App” provides <b>touch friendly and swiping images to
                                your shopify store</b> "the
                                Service" to merchants who use Shopify to power their stores. This Privacy Policy
                                describes how personal information is collected, used, and shared when you install or
                                use the App in connection with your Shopify-supported store.
                            </div>

                            <h3>
                                Personal Information the App Collects
                            </h3>

                            <div>
                                When you install the App, we are automatically able to access certain types of
                                information from your Shopify account: <b>'read_script_tags', 'write_script_tags',
                                'read_products', 'read_themes', 'write_themes'</b>
                            </div>
                            <br/>

                            <div>
                                Additionally, we collect the following types of personal information from you and/or
                                your customers once you have installed the App; Information
                                about individuals who visit your store, such as their IP address, web browser details,
                                time zone, and information about the cookies installed on the particular device.

                                Additionally,
                                we collect the following types of personal information from you and/or your customers
                                once you have installed the App: Information about you and others who
                                may access the App on behalf of your store, such as your name, address, email address,
                                phone number, and billing information; Information about individuals
                                who visit your store, such as their IP address, web browser details, time zone, and
                                information about the cookies installed on the particular device.
                            </div>

                            <br/>

                            <div>
                                We collect personal information directly from the relevant individual, through your
                                Shopify account, or using the following technologies: “Cookies” are
                                data files that are placed on your device or computer and often include an anonymous
                                unique identifier. For more information about cookies, and how to disable cookies, visit
                                http://www.allaboutcookies.org. “Log files” track actions occurring on the Site, and
                                collect data including your IP address, browser type, Internet service provider,
                                referring/exit pages, and date/time stamps. “Web beacons,” “tags,” and “pixels” are
                                electronic files used to record information about how you browse the Site.
                            </div>

                            <h3>
                                How Do We Use Your Personal Information?
                            </h3>

                            <div>
                                We use the personal information we collect from you and your customers in order to
                                provide the Service and to operate the App. Additionally, we use this
                                personal information to: Communicate with you; Optimize or improve the App; and Provide
                                you with information or advertising relating to our products or services.
                            </div>

                            <h3>
                                Sharing Your Personal Information
                            </h3>

                            <div>
                                Finally, we may also share your Personal Information to comply with applicable laws and
                                regulations, to respond to a subpoena, search warrant or other lawful request for
                                information we receive, or to otherwise protect our rights.
                            </div>

                            <br/>

                            <div>
                                Your Rights If you are a European resident, you have the right to access personal
                                information we hold about you and to ask that your personal information be corrected,
                                updated, or deleted. If you would like to exercise this right, please contact us through
                                the contact information below.
                            </div>

                            <br/>

                            <div>
                                Additionally, if you are a European resident we note that we are processing your
                                information in order to fulfill contracts we might have with you (for example if you
                                make an order through the Site), or otherwise to pursue our legitimate business
                                interests listed above. Additionally, please note that your information will be
                                transferred outside of Europe, including to Canada and the United States.
                            </div>

                            <br/>

                            <div>
                                Contact Us For more information about our privacy practices, if you have questions, or
                                if you would like to make a complaint, please contact us by e-mail at
                                gerrone.matticks@gmail.com or by mail using the details provided below:
                            </div>

                            <br/>

                            <div>
                                <p>
                                    Gerrone Matticks <br/>
                                    PRAGMATTICK CONSULTING LIMITED <br/>
                                    First Floor, Telecom House, 125- 135 Preston Road, Brighton, England, BN1 6AF
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={1} sm={0}/>
                </Row>
            </div>
        </div>
    );
};