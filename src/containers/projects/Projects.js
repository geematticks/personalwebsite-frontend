import React, {Component} from 'react';
import projectTitle from "../../assets/img/projects.png";
import {Col, Row} from "react-bootstrap";

import tripsavviLogo from '../../assets/img/projects_tripsavvi.png';
import horizonLogo from '../../assets/img/projects.horizon.png';


import horizonAnimation from '../../assets/img/horizon-animation.gif'
import tripsavviAnimation from '../../assets/img/tripsavvi-animation.gif'

class Projects extends Component {
    render() {
        return (
            <div className='dark-page' style={{minHeight: 'auto'}}>
                <div className="title-holder">
                    <img src={projectTitle} id="title" className="title" alt="experience"/>
                </div>
                <Row style={{width: '100%', paddingTop: '10px', paddingBottom: '10px'}}>
                    <Col lg={3} md={1} sm={0}/>
                    <Col lg={6} md={10} sm={12}>
                        <div className="project">
                            <div className="project-logo">
                                <a href="https://tripsavvi.co.uk" target="_blank">
                                    <div className="title-holder">
                                        <img src={tripsavviLogo} id="title" className="title" alt="tripsavvi"/>
                                    </div>
                                </a>
                            </div>
                            <div className="project-description">
                                <p>
                                    <ul>
                                        <li><b>Collaborative holiday itinerary creator</b></li>
                                        <li>Plan trips with solo trips, or with friends and family</li>
                                        <li>Search potential destinations based on temperature and attractions</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                        <br/>
                        <div className="project">
                            <a href="https://horizon-goals.com" target="_blank">
                                <div className="show-case-landing project-description">
                                    <img src={tripsavviAnimation} alt="Tripsavvi"/>
                                </div>
                            </a>
                        </div>
                        <hr/>

                        <div className="project">
                            <div className="project-logo">
                                <a href="https://horizon-goals.com" target="_blank">
                                    <div className="title-holder">
                                        <img src={horizonLogo} id="title" className="title" alt="horizon-goals"/>
                                    </div>
                                </a>
                            </div>
                            <div className="project-description">
                                <p>
                                    <ul>
                                        <li><b>Goal and task planner</b></li>
                                        <li>Keep track of short term tasks and long term goals</li>
                                        <li>Stay in sync across all devices</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                        <br/>
                        <div className="project">
                            <a href="https://horizon-goals.com" target="_blank">
                                <div className="show-case-landing project-description">
                                    <img src={horizonAnimation} alt="Horizon goals planner"/>
                                </div>
                            </a>
                        </div>

                        <hr/>


                        <div className="project">
                            <a href="https://apps.shopify.com/partners/pragmattick">
                                <span style={{color: 'white', textAlign: 'center', fontSize: '18px'}}>
                                   Shopify Apps
                                </span>
                            </a>
                        </div>

                    </Col>
                    <Col lg={3} md={1} sm={0}/>
                </Row>
            </div>
        )
    }
}

export default Projects;