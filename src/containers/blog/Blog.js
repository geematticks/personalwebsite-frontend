import React, {Component} from 'react'
import blogTitle from "../../assets/img/blog_title.png";

class Blog extends Component {
    render() {
        return (
            <div className={'dark-page-alt'} style={{minHeight: '100px'}}>
                <div className="title-holder" style={{paddingBottom: '10px'}}>
                    <img src={blogTitle} id="title" className="title"/>
                </div>

                <div className="iframe-container">
                    <iframe id="ytplayer"
                            src="https://www.youtube.com/embed/uKlv5NjKbs4?autoplay=0&rel=0&origin=http://https://www.gerronematticks.co.uk"
                            frameBorder="0"/>
                </div>

                <div className="iframe-container">
                    <iframe id="ytplayer"
                            src="https://www.youtube.com/embed/Y2Cb3114aDo?autoplay=0&rel=0&origin=http://https://www.gerronematticks.co.uk"
                            frameBorder="0"/>
                </div>
            </div>
        )
    }
}

export default Blog