import React, {Component} from 'react';
import {Col, Row} from 'react-bootstrap'

import $ from 'jquery';
import 'jquery.appear';

import HttpUtilService from '../util/HttpUtilService'
import Properties from '../properties/Properties'
import LoadingMessage from '../../components/LoadingMessage'
import DisplayMessage from '../../components/DisplayMessage'

import contactMeTitle from '../../assets/img/contactme_title.png';

class ContactMe extends Component {

    static getDefaultEmailInfo() {
        return {
            name: '',
            emailAddress: '',
            subject: '',
            messageText: ''
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            emailInfo: ContactMe.getDefaultEmailInfo(),
            busy: false,
            message: {
                type: null,
                text: null
            }
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDismissAlert = this.handleDismissAlert.bind(this);
    }

    render() {
        return (
            <div style={{minHeight: '650px'}}>
                <div className="title-holder">
                    <img src={contactMeTitle} id="title" className="title"/>
                </div>

                <Row style={{width: '100%'}}>
                    <Col lg={3} md={1} sm={0}/>
                    <Col lg={6} md={10} sm={12}>

                        <p className="center-white-text">
                            Thanks for visiting my page. <br/>
                            Feel free to get in touch by using the form below and I will get back to you as soon as
                            possible.
                        </p>

                        <form onSubmit={this.handleSubmit}>
                            <div className="col-sm-7 col-xs-8  contract-me-form">
                                <input className="form-control" id="name" type="text" placeholder="Your Name"
                                       value={this.state.emailInfo.name}
                                       onChange={e => this.handleChange('name', e)}
                                       disabled={this.state.busy}/>

                                <input className="form-control" id="email" type="text"
                                       placeholder="Your Email Address" value={this.state.emailInfo.emailAddress}
                                       onChange={e => this.handleChange('emailAddress', e)}
                                       disabled={this.state.busy}/>

                                <select className="form-control" id="subject" type="text"
                                        value={this.state.emailInfo.subject} placeholder="Your Message Subject"
                                        onChange={e => this.handleChange('subject', e)}
                                        disabled={this.state.busy}>
                                    <option value="" disabled>Your Message Reason</option>
                                    <option value="Business Enquiry">Business Enquiry</option>
                                    <option value="General Enquiry">General Enquiry</option>
                                    <option value="Website Feedback">Website Suggestion</option>
                                </select>

                                <textarea rows="5" className="form-control" id="message" type="text"
                                          value={this.state.emailInfo.messageText} placeholder="Your Message"
                                          onChange={e => this.handleChange('messageText', e)}
                                          disabled={this.state.busy}/>

                                <input type="submit" value="Submit" className="btn btn-primary btn-lg"
                                       disabled={this.state.busy}/>
                                <LoadingMessage message="Sending..." isLoading={this.state.busy}/>
                                <DisplayMessage message={this.state.message.text} messageType={this.state.message.type}
                                                onDismiss={this.handleDismissAlert}/>
                            </div>
                        </form>

                    </Col>
                    <Col lg={3} md={1} sm={0}/>
                </Row>

            </div>
        );
    }

    componentDidMount() {
        let selector = $('.contract-me-form > input, .contract-me-form > select, .contract-me-form > textarea');
        selector.css('visibility', 'hidden');
        selector.appear(function () {
            let someTransition = (Math.random() * (1000 - 1500) + 1500);
            $(this).css('visibility', 'visible').hide().fadeIn(someTransition);
        });
    }

    handleChange(field, event) {
        let emailInfo = this.state.emailInfo;

        emailInfo[field] = event.target.value;
        this.setState({emailInfo: emailInfo});
    }

    handleSubmit(event) {
        console.log('Sending message containing with details: ' + JSON.stringify(this.state.emailInfo));

        this.setState({busy: true});

        HttpUtilService.post(Properties.ENDPOINT_URL_CONTACT_ME_EMAIL, this.state.emailInfo)
            .then(
                (response) => {
                    console.log("success " + JSON.stringify(response));
                    this.setState({
                        busy: false,
                        message: {type: 'info', text: 'Thanks for your message. I\'ll be in touch!'}
                    });
                },
                (error) => {
                    console.error("success " + JSON.stringify(error));
                    this.setState({
                        busy: false,
                        message: {
                            type: 'danger',
                            text: 'Uh oh, look\'s like something isn\'t working! Sorry for the inconvenience'
                        }
                    });
                })
            .then(() => {
                this.setState({emailInfo: ContactMe.getDefaultEmailInfo()});
            });

        event.preventDefault();
    }

    handleDismissAlert(event) {
        this.setState({
            message: {type: null, text: null}
        });

        event.preventDefault();
    }

}

export default ContactMe;