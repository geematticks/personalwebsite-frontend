import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import './assets/css/main.css';
import './assets/css/mobile.css';

import PageNotFound from './containers/misc/PageNotFound';
import rotateImg from './assets/img/rotate_phone.png';
import HomeScreen from "./containers/homescreen/HomeScreen";
import AboutMe from "./containers/aboutme/AboutMe";
import ContactMe from "./containers/contactme/ContactMe";
import Projects from "./containers/projects/Projects";
import Blog from "./containers/blog/Blog";
import {ShippingDeliveryDeadlinePage} from "./containers/app/ShippingDeliveryDeadlinePage";
import {MobileSwipeImages} from "./containers/app/MobileSwipeImages";
import {DynamicAnnouncementBar} from "./containers/app/DynamicAnnouncementBar";
import {DynamicBundlesSync} from "./containers/app/DynamicBundlesSync";

class App extends Component {
    render() {
        return (
            <div className="mainpage-holder">
                <div className="mainpage-background">
                    <div className="landscape-mode-warning">
                        <img src={rotateImg} id="rotateImg"/>
                    </div>
                    <BrowserRouter>
                        <Switch>
                            <Route exact path="/" component={AppMainPage}/>
                            <Route exact path="/shipping-delivery-deadline" component={ShippingDeliveryDeadlinePage}/>
                            <Route exact path="/touch-and-swipe" component={MobileSwipeImages}/>
                            <Route exact path="/dynamic-announcement-bar" component={DynamicAnnouncementBar}/>
                            <Route exact path="/dynamic-bundles-sync" component={DynamicBundlesSync}/>
                            <Route component={PageNotFound}/>
                        </Switch>
                    </BrowserRouter>
                </div>
            </div>
        );
    }
}

const AppMainPage = () => (
    <React.Fragment>
        <HomeScreen/>
        <Projects/>
        <AboutMe/>
        <Blog/>
        <ContactMe/>
    </React.Fragment>
);

export default App;
